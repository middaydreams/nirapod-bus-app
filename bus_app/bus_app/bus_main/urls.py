from django.urls import path
from . import views

urlpatterns = [
    path('',views.index, name='home'),
    path('apply',views.submit_app_form, name='apply'),
    path('apply-driving-license',views.apply_driving_license, name='apply-driving-license'),
    path('view-application-status',views.view_application, name='view-application-status'),
    path('apply-fitness-certificate',views.apply_fitness_certificate, name='apply-fitness-certificate'),
    path('view-fitness-application-status',views.view_fitness_application, name='view-fitness-application-status'),
]

