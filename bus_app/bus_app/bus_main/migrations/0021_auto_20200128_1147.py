# Generated by Django 2.2.4 on 2020-01-28 11:47

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bus_main', '0020_auto_20200127_1826'),
    ]

    operations = [
        migrations.AlterField(
            model_name='drivinglicense',
            name='apply_time',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 28, 11, 47, 11, 639966)),
        ),
        migrations.AlterField(
            model_name='exammarks',
            name='entry_time',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 28, 11, 47, 11, 637967)),
        ),
        migrations.AlterField(
            model_name='fitnessapplication',
            name='apply_time',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 28, 11, 47, 11, 640965)),
        ),
        migrations.AlterField(
            model_name='learnerapplication',
            name='apply_time',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 28, 11, 47, 11, 635968)),
        ),
        migrations.AlterField(
            model_name='minimummarks',
            name='entry_time',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 28, 11, 47, 11, 636968)),
        ),
    ]
