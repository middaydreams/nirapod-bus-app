# Generated by Django 2.2.4 on 2020-01-23 17:29

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('bus_main', '0002_auto_20200123_1648'),
    ]

    operations = [
        migrations.AddField(
            model_name='learnerapplication',
            name='approved_by',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='approved_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='learnerapplication',
            name='flag',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='learnerapplication',
            name='apply_time',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 23, 17, 29, 1, 154959)),
        ),
    ]
