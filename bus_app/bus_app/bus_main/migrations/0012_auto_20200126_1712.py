# Generated by Django 2.2.4 on 2020-01-26 17:12

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('bus_main', '0011_auto_20200126_1249'),
    ]

    operations = [
        migrations.AlterField(
            model_name='drivinglicense',
            name='apply_time',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 26, 17, 12, 13, 931300)),
        ),
        migrations.AlterField(
            model_name='drivinglicense',
            name='approve_time',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 26, 17, 12, 13, 931300)),
        ),
        migrations.AlterField(
            model_name='exammarks',
            name='entry_time',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 26, 17, 12, 13, 930303)),
        ),
        migrations.AlterField(
            model_name='learnerapplication',
            name='apply_time',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 26, 17, 12, 13, 929308)),
        ),
        migrations.CreateModel(
            name='MinimumMarks',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('written_marks', models.FloatField()),
                ('field_test_marks', models.FloatField()),
                ('viva_marks', models.FloatField()),
                ('entry_time', models.DateTimeField(default=datetime.datetime(2020, 1, 26, 17, 12, 13, 930303))),
                ('entry_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='minimum_marks_entry', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
