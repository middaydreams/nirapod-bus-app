from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.utils.crypto import get_random_string
from datetime import datetime
from .models import LearnerApplication, ExamMarks, DrivingLicense, FitnessApplication, VehicleType
import re



def index(request):
    return redirect('bus:apply')


def submit_app_form(request):
	if request.method=="POST" and request.is_ajax():
		name = request.POST.get("name","").strip()
		contact_no = request.POST.get("contact_no","").strip()
		present_address = request.POST.get("present_address","")
		permanent_address = request.POST.get("permanent_address","")
		national_id = request.POST.get("national_id","").strip()
		tracking_code = get_random_string(length=12)
		if len(name)==0:
			return JsonResponse({'status':4})
		if bool(re.match('^[a-zA-Z- .]+$',name)) == False:
			return JsonResponse({'status':5})
		if len(name)>=66:
			return JsonResponse({'status':6})
		if len(contact_no)==0:
			return JsonResponse({'status':7})
		if len(contact_no)!=11:
			return JsonResponse({'status':8})
		if contact_no[0:2]!="01":
			return JsonResponse({'status':9})
		if bool(re.match('^[0-9]+$',contact_no)) == False:
			return JsonResponse({'status':10})
		if len(permanent_address)==0:
			return JsonResponse({'status':11})
		if len(national_id)==0:
			return JsonResponse({'status':12})
		if bool(re.match('^[0-9]+$',national_id)) == False:
			return JsonResponse({'status':13})
		try:
			an_apply_form = LearnerApplication(name=name, permanent_address=permanent_address,present_address=present_address,
					contact_no=contact_no,tracking_code=tracking_code, national_id=national_id)
			an_apply_form.save()
		except Exception as e:
			print(e)
			return JsonResponse({'status':2})
		try:
			an_apply_form.driver_image = request.FILES['driver_image']
			an_apply_form.nid_image = request.FILES['nid']
			an_apply_form.testimonial = request.FILES['testimonial']
			an_apply_form.apply_form = request.FILES['apply_form']
		except Exception as e:
			print(e)
			return JsonResponse({'status':2})
		an_apply_form.save()
		
		try:
			return JsonResponse({'status':1, 'tracking_id':str(an_apply_form.pk), 'tracking_code':tracking_code})
		except Exception as e:
			return JsonResponse({'status':2})
	return render(request, "bus_main/apply_form.html",{})


def view_application(request):
	if request.method=="GET" and request.is_ajax():		
		try:
			application_id = int(request.GET.get("application",""))
			code = request.GET.get("code","")
			flag_ = request.GET.get("flag","")
			licenseNo = request.GET.get("license_no","")
			learner_application = LearnerApplication.objects.get(pk=application_id, tracking_code=code)
			if flag_=='0' and learner_application.flag>0:
				return JsonResponse({'status':2})
			if flag_=="1":
				learner_application = LearnerApplication.objects.get(pk=application_id, tracking_code=code,license_number=licenseNo)
				if learner_application.flag==6:
					driving_license = DrivingLicense.objects.get(learner_application=learner_application)
					return render(request,"bus_main/application_status.html", {'learner_app':learner_application,'flag':1,
							 'license_no':driving_license.license_no, 'issued_on':driving_license.approve_time})
			return render(request,"bus_main/application_status.html", {'learner_app':learner_application, 'flag':0})
		except Exception as e:
			print(e)
			return JsonResponse({'status':1})
	return render(request, "bus_main/view_learner_application.html",{})


def apply_driving_license(request):
	if request.method=="POST" and request.is_ajax():
		application_id = request.POST.get("application","").strip()
		tracking_code = request.POST.get("code","").strip()
		license_no = request.POST.get("license_no","").strip()
		
		if len(application_id)==0:
			return JsonResponse({'status':4})
		if len(tracking_code)==0:
			return JsonResponse({'status':5})
		if len(license_no)==0:
			return JsonResponse({'status':6})
		try:
			learner_application = LearnerApplication.objects.get(id=application_id, tracking_code=tracking_code, license_number=license_no)
			if learner_application.flag<4:
				return JsonResponse({'status':8})	
		except Exception as e:
			print(e)
			return JsonResponse({'status':7})
		try:
			if DrivingLicense.objects.filter(learner_application=learner_application).count()>0:
				return JsonResponse({'status':9})
			an_apply_form = DrivingLicense(apply_time=datetime.now(),learner_application=learner_application)
			an_apply_form.save()
			learner_application.flag=5
			learner_application.save()
		except Exception as e:
			print(e)
			return JsonResponse({'status':2})
		try:
			an_apply_form.driver_image = request.FILES['driver_image']
			an_apply_form.driver_signature = request.FILES['signature']
			an_apply_form.apply_form = request.FILES['apply_form']
			an_apply_form.learner_license = request.FILES['learner_license']
		except Exception as e:
			print(e)
			return JsonResponse({'status':2})
		an_apply_form.save()
		
		return JsonResponse({'status':1})
	return render(request, "bus_main/apply_driving_license.html",{})


def apply_fitness_certificate(request):
	if request.method=="POST" and request.is_ajax():
		name = request.POST.get("name","").strip()
		contact_no = request.POST.get("contact_no","").strip()
		registration_no = request.POST.get("registration_no","").strip()
		vehicle_type = request.POST.get("vehicle_type","")
		fitness_certi_no = request.POST.get("fitness_certificate_no","")
		tracking_code = get_random_string(length=12)
		tracking_flag = True
		while tracking_flag:
			if FitnessApplication.objects.filter(tracking_code=tracking_code).count()>0:
				tracking_code = get_random_string(length=12)
			else:
				tracking_flag = False
		if len(name)==0:
			return JsonResponse({'status':4})
		if bool(re.match('^[a-zA-Z- .]+$',name)) == False:
			return JsonResponse({'status':5})
		if len(name)>=66:
			return JsonResponse({'status':6})
		if len(contact_no)==0:
			return JsonResponse({'status':7})
		if len(contact_no)!=11:
			return JsonResponse({'status':8})
		if contact_no[0:2]!="01":
			return JsonResponse({'status':9})
		if bool(re.match('^[0-9]+$',contact_no)) == False:
			return JsonResponse({'status':10})
		if len(registration_no)==0:
			return JsonResponse({'status':11})
		if len(vehicle_type)==0:
			return JsonResponse({'status':12})
		if len(fitness_certi_no)==0:
			return JsonResponse({'status':13})

		try:
			an_apply_form = FitnessApplication(name=name, registration_no=registration_no,contact_no=contact_no,fitness_certificate_no=fitness_certi_no,
				tracking_code=tracking_code,apply_time=datetime.now(), vehicle_type=VehicleType.objects.get(id=int(vehicle_type)))
			#an_apply_form.save()
		except Exception as e:
			print(e)
			return JsonResponse({'status':2})
		try:
			an_apply_form.bank_receipt = request.FILES['bank_receipt']
			an_apply_form.fitness_certificate = request.FILES['fitness_certificate']
		except Exception as e:
			print(e)
			return JsonResponse({'status':2})
		an_apply_form.save()
		
		try:
			return JsonResponse({'status':1, 'tracking_id':str(an_apply_form.pk), 'tracking_code':tracking_code})
		except Exception as e:
			return JsonResponse({'status':2})
	
	vehicle_types = VehicleType.objects.all().order_by('name')
	return render(request, "bus_main/apply_fitness_certificate.html",{'vehicle_types':vehicle_types})
	

def view_fitness_application(request):
	if request.method=="GET" and request.is_ajax():		
		try:
			application_id = int(request.GET.get("application",""))
			code = request.GET.get("code","")
			certificateNo = request.GET.get("certificate_no","")
			fitness_application = FitnessApplication.objects.get(pk=application_id, tracking_code=code, fitness_certificate_no=certificateNo)
			
			return render(request,"bus_main/fitness_status.html", {'fitness_app':fitness_application})
		except Exception as e:
			print(e)
			return JsonResponse({'status':1})
	return render(request, "bus_main/view_fitness_application.html",{})