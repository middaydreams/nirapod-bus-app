from django.template import Library

register = Library()

@register.filter
def sort_by_ascending(queryset):
    return queryset.order_by('id')

@register.filter
def sort_by_descending(queryset):
    return queryset.order_by('-id')
