from django.conf import settings



def default_bus_context(request):
    default_context = {
        'static_server':settings.STATIC_SERVER,
        'maintheme_server':settings.MAINTHEME_SERVER,
		'media_server':settings.MEDIA_SERVER,
        'main_server':settings.MAIN_SERVER
    }
    return default_context


