from django import forms
from datetime import datetime
from django.db import models
from django.contrib.auth.models import User
import os, uuid

'''
flag==>LearnerApplication
0--Applied for Learner Application, 1--Approved for Learner License, 2--Ready for Driving License Exam,
3--Appeared in Driving License Exam (Failed), 4--Appeared in Driving License Exam (Passed),
5--Applied for Driving License, 6--Driving License has been issued

'''

class LearnerApplication(models.Model):
    def driver_image_file(instance, filename):
        ext = filename.split('.')[-1]
        filename = "%s.%s" % (uuid.uuid4(), ext)
        return os.path.join('files/person-image/', filename)
    def random_id_file_name(instance, filename):
        ext = filename.split('.')[-1]
        filename = "%s.%s" % (uuid.uuid4(), ext)
        return os.path.join('files/nid/', filename)
    
    def random_testimonial_file_name(instance, filename):
        ext = filename.split('.')[-1]
        filename = "%s.%s" % (uuid.uuid4(), ext)
        return os.path.join('files/testimonial/', filename)

    def random_form_file_name(instance, filename):
        ext = filename.split('.')[-1]
        filename = "%s.%s" % (uuid.uuid4(), ext)
        return os.path.join('files/application-form/', filename)

    flag_types = ["Applied for Learner Application", "Approved for Learner License","Ready for Driving License Exam",
                 "Appeared in Driving License Exam (Failed)", "Appeared in Driving License Exam (Passed)", "Applied for Driving License",
                 "Driving License has been issued"]

    name = models.TextField(default="")
    contact_no = models.CharField(max_length=255, default="")
    flag = models.IntegerField(default=0)
    approved_by = models.ForeignKey(User, related_name="approved_learner_application", null=True, on_delete=models.SET_NULL)
    marked_ready_by = models.ForeignKey(User, related_name="marked_ready", null=True, on_delete=models.SET_NULL)
    present_address = models.TextField(default="")
    permanent_address = models.TextField(default="")
    tracking_code = models.TextField(default="")
    national_id = models.TextField(default="")
    driver_image = models.FileField(upload_to=driver_image_file, blank=True, null=True, default="")
    nid_image = models.FileField(upload_to=random_id_file_name, blank=True, null=True, default="")
    testimonial = models.FileField(upload_to=random_testimonial_file_name, blank=True, null=True, default="")
    apply_form = models.FileField(upload_to=random_form_file_name, blank=True, null=True, default="")
    #status = models.IntegerField(default=0)
    apply_time = models.DateTimeField(default=datetime.now())
    approve_time = models.DateTimeField(null=True)
    ready_time = models.DateTimeField(null=True)
    license_number = models.BigIntegerField(default=0)

    @property
    def application_status(self):
        return self.flag_types[self.flag]



class MinimumMarks(models.Model):
    written_marks = models.FloatField()
    field_test_marks = models.FloatField()
    viva_marks = models.FloatField()
    entry_time = models.DateTimeField(default=datetime.now())
    entry_by = models.ForeignKey(User, related_name="minimum_marks_entry", null=True, on_delete=models.SET_NULL)



class ExamMarks(models.Model):
    written_marks = models.FloatField()
    field_test_marks = models.FloatField()
    viva_marks = models.FloatField()
    written_exam_date = models.DateField(null=True)
    field_test_date = models.DateField(null=True)
    viva_exam_date = models.DateField(null=True)
    entry_time = models.DateTimeField(default=datetime.now())
    has_passed = models.BooleanField(default=False)
    entry_by = models.ForeignKey(User, related_name="exam_marks_entry", null=True, on_delete=models.SET_NULL)
    learner_application = models.ForeignKey(LearnerApplication, related_name="exam_marks", null=True, on_delete=models.SET_NULL)


class DrivingLicense(models.Model):
    def driver_image_file(instance, filename):
        ext = filename.split('.')[-1]
        filename = "%s.%s" % (uuid.uuid4(), ext)
        return os.path.join('files/driver-image/', filename)
    
    def apply_form_file(instance, filename):
        ext = filename.split('.')[-1]
        filename = "%s.%s" % (uuid.uuid4(), ext)
        return os.path.join('files/driving-license-application-form/', filename)

    def driver_signature_file(instance, filename):
        ext = filename.split('.')[-1]
        filename = "%s.%s" % (uuid.uuid4(), ext)
        return os.path.join('files/driver-signature/', filename)
    
    def learner_license_file(instance, filename):
        ext = filename.split('.')[-1]
        filename = "%s.%s" % (uuid.uuid4(), ext)
        return os.path.join('files/learner-license/', filename)

    is_paid = models.BooleanField(default=False)
    pay_date = models.DateField(null=True)
    license_no = models.BigIntegerField(default=0)
    driver_finger_print = models.TextField(default="")
    driver_image = models.FileField(upload_to=driver_image_file, blank=True, null=True, default="")
    driver_signature = models.FileField(upload_to=driver_signature_file, blank=True, null=True, default="")
    apply_form = models.FileField(upload_to=apply_form_file, blank=True, null=True, default="")
    learner_license = models.FileField(upload_to=learner_license_file, blank=True, null=True, default="")
    apply_time = models.DateTimeField(default=datetime.now())
    approved_by = models.ForeignKey(User, related_name="approved_driving_license", null=True, on_delete=models.SET_NULL)
    approve_time = models.DateTimeField(null=True)    
    learner_application = models.ForeignKey(LearnerApplication, related_name="driving_license_learner_app", null=True, on_delete=models.SET_NULL)
    


class VehicleType(models.Model):
    name = models.TextField(max_length=255)



class FitnessApplication(models.Model):
    def bank_receipt_file_name(instance, filename):
        ext = filename.split('.')[-1]
        filename = "%s.%s" % (uuid.uuid4(), ext)
        return os.path.join('files/bank-receipt/', filename)

    def fitness_certificate_file_name(instance, filename):
        ext = filename.split('.')[-1]
        filename = "%s.%s" % (uuid.uuid4(), ext)
        return os.path.join('files/fitness-certificate/', filename)
    flag_types = ["Applied for Fitness Status", "Assigned to Inspector","Approved by Inspector",
                 "Fitness Certificate has been Issued"]

    name = models.TextField(max_length=255)
    contact_no = models.CharField(max_length=255, default="")
    registration_no = models.TextField(default="")
    flag = models.IntegerField(default=0)
    bank_receipt = models.FileField(upload_to=bank_receipt_file_name, blank=True, null=True, default="")
    fitness_certificate = models.FileField(upload_to=fitness_certificate_file_name, blank=True, null=True, default="")
    tracking_code = models.TextField(default="")
    fitness_certificate_no = models.TextField(default="")
    apply_time = models.DateTimeField(default=datetime.now())
    inspection_time = models.DateTimeField(null=True)
    approve_time = models.DateTimeField(null=True)
    assign_time = models.DateTimeField(null=True)
    inspection_date = models.DateField(null=True)
    vehicle_type = models.ForeignKey(VehicleType, related_name="vehicle_fitness_application", null=True, on_delete=models.SET_NULL)
    approved_by = models.ForeignKey(User, related_name="approved_fitness_application", null=True, on_delete=models.SET_NULL)
    assigned_by = models.ForeignKey(User, related_name="assigned_inspector", null=True, on_delete=models.SET_NULL)
    inspected_by = models.ForeignKey(User, related_name="fitness_application", null=True, on_delete=models.SET_NULL)
    remarks = models.TextField(null=True)
    #status = models.IntegerField(default=0)
    
    @property
    def application_status(self):
        return self.flag_types[self.flag]

