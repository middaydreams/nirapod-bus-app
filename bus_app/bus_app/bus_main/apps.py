from django.apps import AppConfig


class BusMainConfig(AppConfig):
    name = 'bus_main'
