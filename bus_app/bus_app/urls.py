from django.contrib import admin
from django.urls import path, include
import bus_app.bus_main.urls as main_urls
import bus_app.account.urls as account_urls
import bus_app.dashboard.urls as dashboard_urls
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from django.views.static import serve


#handler404 = 'blog_site.blog_main.views.handle_404'


urlpatterns = [
	path('', include((main_urls, 'bus'), namespace='bus')),
	path('account/', include((account_urls, 'account'), namespace='account')),
	path('dashboard/',include((dashboard_urls, 'dashboard'), namespace='dashboard')),
    path('default-admin-dashboard/', admin.site.urls),
	url(r'^media/(?P<path>.*)$', serve,{'document_root': settings.MEDIA_ROOT}),
	url(r'^static/(?P<path>.*)$', serve,{'document_root': "static"}),
]

urlpatterns += static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root="static")

