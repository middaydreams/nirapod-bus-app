
from datetime import datetime
from django.db import models
from django.contrib.auth.models import User


class AppUser(models.Model):
    role = models.IntegerField()
    contact_no = models.TextField(max_length=14)
    user = models.ForeignKey(User, related_name="appuser", null=True, on_delete=models.SET_NULL)
    entry_by = models.ForeignKey(User, related_name="appuser_entry", null=True, on_delete=models.SET_NULL)


