from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib import auth
from django.http import JsonResponse
from django.core.paginator import Paginator
from datetime import datetime
from django.db.models import Max
from ..decorators import login_required, superuser_required
from ..account.models import AppUser
from ..bus_main.models import LearnerApplication, MinimumMarks, ExamMarks, VehicleType, DrivingLicense, FitnessApplication
import re

def login(request):
    if request.user.is_authenticated:
        return redirect('dashboard:home')
    if request.is_ajax() and request.method == "POST":
        username = request.POST.get("username", "")
        password = request.POST.get("password", "")
        if len(username) == 0:
            return JsonResponse({'status':0})
        elif len(password) == 0:
            return JsonResponse({'status':2})
        else:
            user = auth.authenticate(username=username.lower(), password=password)
            if user is not None:
                auth.login(request, user)
                return JsonResponse({'status':1})
    return render(request,'dashboard/login.html')



#@superuser_required
def index(request):
    if not request.user.is_authenticated:
        return redirect('bus:home')
    
    context = {}
    if request.user.is_superuser:
        context.update({
            'learner_applications':str(LearnerApplication.objects.filter(flag=0).count()),
            'approved_for_learner':str(LearnerApplication.objects.filter(flag=1).count()),
            'ready_for_exam':str(LearnerApplication.objects.filter(flag=2).count()),
            'passed_in_exam':str(LearnerApplication.objects.filter(flag=4).count()),
            'applied_for_driving_license':str(LearnerApplication.objects.filter(flag=5).count()),
            'issued_licenses':str(LearnerApplication.objects.filter(flag=6).count()),
            'fitness_applications':str(FitnessApplication.objects.filter(flag=0).count()),
            'approved_by_inspectors':str(FitnessApplication.objects.filter(flag=2).count()),
            'issued_certificates':str(FitnessApplication.objects.filter(flag=3).count()),
        })
    else:
        context.update({
            'assigned_to_me':str(FitnessApplication.objects.filter(flag=1,inspected_by=request.user).count()),
            'approved_by_me':str(FitnessApplication.objects.filter(flag__gte=2,inspected_by=request.user).count()),
        })
    return render(request,'dashboard/index.html', context)


@superuser_required
def learner_applications(request):
    try:
        learner_applications = Paginator(LearnerApplication.objects.filter(flag=0).order_by('apply_time'),20).page(request.GET.get("page",1))
        return render(request,'dashboard/learner_applications.html', {'learner_applications':learner_applications})
    except Exception as e:
        print(e)
        return redirect('dashboard:home')



@superuser_required
def learner_details(request):
    if request.is_ajax():
        try:
            an_application = LearnerApplication.objects.get(id=int(request.GET.get("learner_id","")))
            context = {
                'an_application':an_application
            }
            if 'view_exam_history' in request.GET:
                context.update({
                    'view_exam_history':True
                })
            if an_application.flag>=5:
                try:
                    driving_application = DrivingLicense.objects.get(learner_application=an_application)
                    context.update({
                        'driving_application':driving_application
                    })
                except Exception as e:
                    print(e)
            return render(request,'dashboard/learner_details.html', context)
        except Exception as e:
            print(e)
    return redirect('dashboard:home')



@superuser_required
def approve_learner(request):
    if request.is_ajax():
        try:
            an_application = LearnerApplication.objects.get(id=int(request.POST.get("learner_id","")))
            if an_application.flag==0:
                an_application.flag = 1
                max_license_number = LearnerApplication.objects.aggregate(Max('license_number'))['license_number__max']
                if max_license_number==0:
                    an_application.license_number = 1000000
                else:
                    an_application.license_number = max_license_number+1
                an_application.approve_time = datetime.now()
                an_application.approved_by = request.user
                an_application.save()
            return JsonResponse({'status':1})
        except Exception as e:
            print(e)
            return JsonResponse({'status':0})
    return redirect('dashboard:home')


@superuser_required
def approved_for_learner(request):
    try:
        learner_applications = Paginator(LearnerApplication.objects.filter(flag=1).order_by('apply_time'),20).page(request.GET.get("page",1))
        return render(request,'dashboard/approved-for-learner.html', {'learner_applications':learner_applications})
    except Exception as e:
        print(e)
        return redirect('dashboard:home')


@superuser_required
def mark_ready(request):
    if request.is_ajax():
        try:
            an_application = LearnerApplication.objects.get(id=int(request.POST.get("learner_id","")))
            if an_application.flag==1:
                an_application.flag = 2
                an_application.ready_time = datetime.now()
                an_application.marked_ready_by = request.user
                an_application.save()
            return JsonResponse({'status':1})
        except Exception as e:
            print(e)
            return JsonResponse({'status':0})
    return redirect('dashboard:home')


@superuser_required
def ready_for_exam(request):
    try:
        learner_applications = Paginator(LearnerApplication.objects.filter(flag=2).order_by('apply_time'),20).page(request.GET.get("page",1))
        return render(request,'dashboard/ready-for-exam.html', {'learner_applications':learner_applications})
    except Exception as e:
        print(e)
        return redirect('dashboard:home')


import math
def mark_setup(request):
        minimum_marks = MinimumMarks.objects.first()
        if request.is_ajax() and request.method=="POST":
            try:
                written_marks = float(request.POST.get("written_marks", ""))
                field_test_marks = float(request.POST.get("field_test_marks", ""))
                viva_marks = float(request.POST.get("viva_marks", ""))
                if math.ceil(written_marks)==0 or math.ceil(field_test_marks)==0 or math.ceil(viva_marks)==0:
                    return JsonResponse({'status':0})
            except Exception as e:
                print(e)
                return JsonResponse({'status':0})
            if MinimumMarks.objects.all().count()>0:
                MinimumMarks.objects.update(written_marks=written_marks, field_test_marks=field_test_marks, viva_marks=viva_marks, entry_time=datetime.now(), entry_by=request.user)
            else:
                MinimumMarks(written_marks=written_marks, field_test_marks=field_test_marks, viva_marks=viva_marks, entry_time=datetime.now(), entry_by=request.user).save()
            return JsonResponse({'status':1})
        return render(request,'dashboard/mark-setup.html', {'minimum_marks':minimum_marks})



@superuser_required
def submit_marks(request):
    if request.is_ajax():
        try:
            if MinimumMarks.objects.all().count()==0:
                return JsonResponse({'status':4})
            an_application = LearnerApplication.objects.get(id=int(request.POST.get("learner_id","")))
            if an_application.flag<2:
                return JsonResponse({'status':3})
            if an_application.flag>=4:
                return JsonResponse({'status':5})
            written_marks = request.POST.get("written_marks", "")
            written_exam_date = request.POST.get("written_date", "")
            field_test_marks = request.POST.get("field_test_marks", "")
            field_test_date = request.POST.get("field_test_date", "")
            viva_marks = request.POST.get("viva_marks", "")
            viva_exam_date = request.POST.get("viva_date", "")
            if len(written_marks)==0 or len(written_exam_date)==0 or len(field_test_marks)==0 or len(field_test_date)==0 or len(viva_marks)==0 or len(viva_exam_date)==0:
                return JsonResponse({'status':0})   
            exam_marks = ExamMarks(written_marks=written_marks, written_exam_date=written_exam_date, field_test_marks=field_test_marks, field_test_date=field_test_date, viva_marks=viva_marks, viva_exam_date=viva_exam_date, entry_time=datetime.now(), entry_by=request.user, learner_application=an_application)
            minimum_marks = MinimumMarks.objects.first()
            if float(written_marks)<minimum_marks.written_marks or float(field_test_marks)<minimum_marks.field_test_marks or float(viva_marks)<minimum_marks.viva_marks:
                an_application.flag = 3
                exam_marks.has_passed = False
            else:
                an_application.flag = 4
                exam_marks.has_passed = True
            exam_marks.save()
            an_application.save()
            return JsonResponse({'status':1, 'has_passed':exam_marks.has_passed})
        except Exception as e:
            print(e)
            return JsonResponse({'status':0})
    return redirect('dashboard:home')


@superuser_required
def failed_in_exam(request):
    try:
        learner_applications = Paginator(LearnerApplication.objects.filter(flag=3).order_by('apply_time'),20).page(request.GET.get("page",1))
        return render(request,'dashboard/failed-in-exam.html', {'learner_applications':learner_applications})
    except Exception as e:
        print(e)
        return redirect('dashboard:home')


@superuser_required
def passed_in_exam(request):
    try:
        learner_applications = Paginator(LearnerApplication.objects.filter(flag=4).order_by('apply_time'),20).page(request.GET.get("page",1))
        return render(request,'dashboard/passed-in-exam.html', {'learner_applications':learner_applications})
    except Exception as e:
        print(e)
        return redirect('dashboard:home')


@superuser_required
def applied_for_driving_license(request):
    try:
        learner_applications = Paginator(LearnerApplication.objects.filter(flag=5).order_by('apply_time'),20).page(request.GET.get("page",1))
        return render(request,'dashboard/applied-for-driving-license.html', {'learner_applications':learner_applications})
    except Exception as e:
        print(e)
        return redirect('dashboard:home')


@superuser_required
def issue_driving_license(request):
    if request.is_ajax() and request.method=="POST":
        try:
            driving_application = DrivingLicense.objects.get(pk=request.POST.get("driver_id",""))
            if driving_application.is_paid:
                return JsonResponse({'status':4})
            driving_application.is_paid = True
            driving_application.pay_date = request.POST.get("pay_date", "")
            print(DrivingLicense.objects.aggregate(Max('license_no'))['license_no__max'])
            max_license_number = DrivingLicense.objects.aggregate(Max('license_no'))['license_no__max']
            if max_license_number==0:
                driving_application.license_no = 2222222
            else:
                driving_application.license_no = max_license_number+1
            driving_application.approved_by = request.user
            driving_application.approve_time = datetime.now()
            driving_application.save()
            learner_application = LearnerApplication.objects.get(pk=driving_application.learner_application.id)
            learner_application.flag = 6
            learner_application.save()
            return JsonResponse({'status':1})
        except Exception as e:
            print(e)
            return JsonResponse({'status':0})
    return redirect('dashboard:home')


@superuser_required
def issued_driving_licenses(request):
    try:
        learner_applications = Paginator(LearnerApplication.objects.filter(flag=6).order_by('apply_time'),20).page(request.GET.get("page",1))
        return render(request,'dashboard/issued-driving-licenses.html', {'learner_applications':learner_applications})
    except Exception as e:
        print(e)
        return redirect('dashboard:home')


@superuser_required
def fitness_applications(request):
    try:
        fitness_applications = Paginator(FitnessApplication.objects.filter(flag=0).order_by('apply_time'),20).page(request.GET.get("page",1))
        return render(request,'dashboard/fitness-applications.html', {'fitness_applications':fitness_applications})
    except Exception as e:
        print(e)
        return redirect('dashboard:home')


#@superuser_required
def fitness_details(request):
    if request.is_ajax():
        try:
            an_application = FitnessApplication.objects.get(id=int(request.GET.get("fitness_id","")))
            if request.user.is_superuser or an_application.inspected_by==request.user:
                context = {
                    'an_application':an_application
                }
                if 'assigned_to_menu' in request.GET:
                    context.update({
                        'assigned_to_menu':True
                    })
                if an_application.flag==0:
                    context.update({
                        'inspectors':AppUser.objects.all()
                    })
                return render(request,'dashboard/fitness_details.html', context)
            return JsonResponse({'status':0})
        except Exception as e:
            print(e)
            return JsonResponse({'status':0})
    return redirect('dashboard:home')

@superuser_required
def assign_inspector(request):
    if request.is_ajax() and request.method=="POST":
        try:
            an_application = FitnessApplication.objects.get(id=int(request.POST.get("fitness_id","")))            
            if an_application.flag==0:
                an_application.inspected_by =  User.objects.get(pk=request.POST.get("inspector_id",""))
                an_application.flag = 1            
                an_application.assigned_by = request.user
                an_application.assign_time = datetime.now()
                an_application.save()
                return JsonResponse({'status':1})
            else:
                return JsonResponse({'status':0})
        except Exception as e:
            print(e)
            return JsonResponse({'status':0})
    return redirect('dashboard:home')


@superuser_required
def assigned_to_inspector(request):
    try:
        fitness_applications = Paginator(FitnessApplication.objects.filter(flag=1).order_by('apply_time'),20).page(request.GET.get("page",1))
        return render(request,'dashboard/assigned-to-inspector.html', {'fitness_applications':fitness_applications})
    except Exception as e:
        print(e)
        return redirect('dashboard:home')


def assigned_to_me(request):
    if not request.user.is_authenticated:
        return redirect('bus:home')
    fitness_applications = Paginator(FitnessApplication.objects.filter(flag=1, inspected_by=request.user).order_by('apply_time'),20).page(request.GET.get("page",1))
    return render(request,'dashboard/assigned-to-me.html', {'fitness_applications':fitness_applications})


def mark_as_inspected(request):
    if not request.user.is_authenticated:
        return redirect('dashboard:home')
    if request.is_ajax() and request.method=="POST":
        try:
            an_application = FitnessApplication.objects.get(id=int(request.POST.get("fitness_id","")))
            if an_application.inspected_by != request.user:
                return JsonResponse({'status':2})
            if an_application.flag==1:
                an_application.flag = 2
                an_application.inspection_date = request.POST.get("inspection_date","")
                an_application.remarks = request.POST.get("remarks","")
                an_application.inspection_time = datetime.now()
                an_application.save()
                return JsonResponse({'status':1})
            else:
                return JsonResponse({'status':0})
        except Exception as e:
            print(e)
            return JsonResponse({'status':0})
    return redirect('dashboard:home')

def inspected_by_me(request):
    if not request.user.is_authenticated:
        return redirect('bus:home')
    fitness_applications = Paginator(FitnessApplication.objects.filter(flag__gte=2, inspected_by=request.user).order_by('apply_time'),20).page(request.GET.get("page",1))
    return render(request,'dashboard/inspected-by-me.html', {'fitness_applications':fitness_applications})

@superuser_required
def approved_by_inspector(request):
    fitness_applications = Paginator(FitnessApplication.objects.filter(flag=2).order_by('apply_time'),20).page(request.GET.get("page",1))
    return render(request,'dashboard/approved-by-inspector.html', {'fitness_applications':fitness_applications})

@superuser_required
def issue_fitness_certificate(request):
    print("sdfsdfsdfsdf")
    if request.is_ajax() and request.method=="POST":
        try:
            an_application = FitnessApplication.objects.get(id=int(request.POST.get("fitness_id","")))
            if an_application.flag==2:
                an_application.flag = 3
                an_application.approved_by = request.user
                an_application.approve_time = datetime.now()
                an_application.save()
                return JsonResponse({'status':1})
            else:
                return JsonResponse({'status':0})
        except Exception as e:
            print(e)
            return JsonResponse({'status':0})
    return redirect('dashboard:home')


@superuser_required
def issued_certificates(request):
    fitness_applications = Paginator(FitnessApplication.objects.filter(flag=3).order_by('apply_time'),20).page(request.GET.get("page",1))
    return render(request,'dashboard/issued-certificates.html', {'fitness_applications':fitness_applications})


@superuser_required
def vehicle_type(request):
    if request.is_ajax() and request.method=="POST":
        try:
            action_ = request.POST.get("action", "")
            vehicle_type = request.POST.get("vehicle_type", "")
            vehicle_type_id = request.POST.get("vehicle_type_id", "")
            if action_=="save":
                if len(vehicle_type)==0:
                    return JsonResponse({'message':2})
                if VehicleType.objects.filter(name__icontains=vehicle_type).count()>0:
                    return JsonResponse({'message':3})
                vehicle = VehicleType(name=vehicle_type)
                vehicle.save()
                return JsonResponse({'message':1, 'id':vehicle.id})
            elif action_=="update":
                if len(vehicle_type)==0:
                    return JsonResponse({'message':2})
                if VehicleType.objects.filter(name__icontains=vehicle_type).exclude(id=int(vehicle_type_id)).count()>0:
                    return JsonResponse({'message':3})
                VehicleType.objects.filter(id=int(vehicle_type_id)).update(name=vehicle_type)
                return JsonResponse({'message':1})
            elif action_=="delete":
                VehicleType.objects.filter(id=int(vehicle_type_id)).delete()
                return JsonResponse({'message':1})
        except Exception as e:
            print(e)
            return JsonResponse({'message':5})
    vehicle_types = VehicleType.objects.all().order_by('-id')
    return render(request,'dashboard/vehicle-type.html', {'vehicle_types':vehicle_types})


@superuser_required
def user_setup(request):
    if request.is_ajax() and request.method=="POST":
        try:
            action_ = request.POST.get("action", "")
            user_id = request.POST.get("user_id", "")
            first_name = request.POST.get("first_name", "")
            last_name = request.POST.get("last_name", "")
            username = request.POST.get("username", "")
            password = request.POST.get("password", "")
            confirm_password = request.POST.get("confirm_password", "")
            contact = request.POST.get("contact_no", "")
            email = request.POST.get("email", "")
            user_role = request.POST.get("role", "")
            if action_=="save":
                if len(first_name)==0:
                    return JsonResponse({'message':2})
                if len(last_name)==0:
                    return JsonResponse({'message':3})
                if len(contact)==0:
                    return JsonResponse({'message':4})
                if len(username)==0:
                    return JsonResponse({'message':5})
                if bool(re.match('^[a-zA-Z0-9-]+$', username)) == False:
                    return JsonResponse({'message':12})
                if len(password)<=5:
                    return JsonResponse({'message':8})
                if password!=confirm_password:
                    return JsonResponse({'message':9})
                if len(user_role)==0:
                    return JsonResponse({'message':6})
                if User.objects.filter(username__icontains=username).count()>0:
                    return JsonResponse({'message':7})
                user_ = User.objects.create_user(first_name=first_name,last_name=last_name,username=username,email=email, password=password)
                if user_role=='1':
                    user_.is_superuser= True
                user_.save()
                app_ = AppUser(role=user_role, contact_no=contact, user=user_, entry_by=request.user)
                app_.save()
                return JsonResponse({'message':1, 'id':user_.id})
            elif action_=="update":
                if len(first_name)==0:
                    return JsonResponse({'message':2})
                if len(last_name)==0:
                    return JsonResponse({'message':3})
                if len(contact)==0:
                    return JsonResponse({'message':4})
                if len(username)==0:
                    return JsonResponse({'message':5})
                if bool(re.match('^[a-zA-Z0-9-]+$', username)) == False:
                    print('error')
                    return JsonResponse({'message':12})
                if len(user_role)==0:
                    return JsonResponse({'message':6})

                if User.objects.filter(username__icontains=username).exclude(id=int(user_id)).count()>0:
                    return JsonResponse({'message':7})
                user_ = User.objects.get(id=int(user_id))
                user_.first_name = first_name
                user_.last_name = last_name
                user_.username = username
                user_.email = email
                if user_role=='1':
                    user_.is_superuser = True
                else:
                    user_.is_superuser = False
                user_.save()
                AppUser.objects.filter(user=int(user_id)).update(contact_no=contact, role=user_role)
                return JsonResponse({'message':1})
            elif action_=="delete":
                User.objects.get(id=int(user_id)).delete()
                return JsonResponse({'message':1})
        except Exception as e:
            print(e)
            return JsonResponse({'message':10})
    appusers_id = AppUser.objects.all().values_list('user_id', flat=True)
    users = User.objects.filter(id__in=appusers_id).order_by('-id')
    return render(request,'dashboard/users.html', {'users':users})


#from django.views.decorators.csrf import csrf_exempt
#@csrf_exempt
def fingerprint_checker(request):
    fingerprint = request.POST.get("fingerprint","No Fingerprint Found")
    return render(request,'dashboard/fingerprint.html', {'fingerprint':fingerprint})

def logout(request):
    auth.logout(request)
    return redirect('dashboard:login')


## Demo Dashboard

#@superuser_required
def brta_dashboard(request):
    if not request.user.is_authenticated:
        return redirect('bus:home')
    
    context = {}
    if request.user.is_superuser:
        context.update({
            'learner_applications':str(LearnerApplication.objects.filter(flag=0).count()),
            'approved_for_learner':str(LearnerApplication.objects.filter(flag=1).count()),
            'ready_for_exam':str(LearnerApplication.objects.filter(flag=2).count()),
            'passed_in_exam':str(LearnerApplication.objects.filter(flag=4).count()),
            'applied_for_driving_license':str(LearnerApplication.objects.filter(flag=5).count()),
            'issued_licenses':str(LearnerApplication.objects.filter(flag=6).count()),
            'fitness_applications':str(FitnessApplication.objects.filter(flag=0).count()),
            'approved_by_inspectors':str(FitnessApplication.objects.filter(flag=2).count()),
            'issued_certificates':str(FitnessApplication.objects.filter(flag=3).count()),
        })
    else:
        context.update({
            'assigned_to_me':str(FitnessApplication.objects.filter(flag=1,inspected_by=request.user).count()),
            'approved_by_me':str(FitnessApplication.objects.filter(flag__gte=2,inspected_by=request.user).count()),
        })
    return render(request,'dashboard/brta_index.html', context)


def bus_owner_dashboard(request):
    return render(request,'dashboard/bus_owner_index.html', {})

def emergency_service_dashboard(request):
    return render(request,'dashboard/999_index.html', {})

def urban_dashboard(request):
    return render(request,'dashboard/urban_index.html', {})