from django.urls import path
from . import views

urlpatterns = [
	path('',views.index, name='home'),
	
	#license process
	path('learner-applications',views.learner_applications, name='learner-applications'),
	path('learner-details',views.learner_details, name='learner-details'),
	path('approve-learner',views.approve_learner, name='approve-learner'),
	path('approved-for-learner',views.approved_for_learner, name='approved-for-learner'),
	path('mark-ready',views.mark_ready, name='mark-ready'),
	path('ready-for-exam',views.ready_for_exam, name='ready-for-exam'),
	path('mark-setup',views.mark_setup, name='mark-setup'),
	path('submit-marks',views.submit_marks, name='submit-marks'),
	path('failed-in-exam',views.failed_in_exam, name='failed-in-exam'),
	path('passed-in-exam',views.passed_in_exam, name='passed-in-exam'),
	path('applied-for-driving-license',views.applied_for_driving_license, name='applied-for-driving-license'),
	path('issue-driving-license',views.issue_driving_license, name='issue-driving-license'),
	path('issued-driving-license',views.issued_driving_licenses, name='issued-driving-license'),
	
	#fitness renewal
	path('fitness-applications',views.fitness_applications, name='fitness-applications'),
	path('fitness-details',views.fitness_details, name='fitness-details'),
	path('assign-inspector',views.assign_inspector, name='assign-inspector'),
	path('assigned-to-inspector',views.assigned_to_inspector, name='assigned-to-inspector'),
	path('assigned-to-me',views.assigned_to_me, name='assigned-to-me'),
	path('mark-is-inspected',views.mark_as_inspected, name='mark-is-inspected'),
	path('inspected-by-me',views.inspected_by_me, name='inspected-by-me'),
	path('approved-by-inspector',views.approved_by_inspector, name='approved-by-inspector'),
	path('issue-fitness-certificate',views.issue_fitness_certificate, name='issue-fitness-certificate'),
	path('issued-certificates',views.issued_certificates, name='issued-certificates'),
	
	
	#setup
	path('vehicle-type',views.vehicle_type, name='vehicle-type'),
	path('user-setup',views.user_setup, name='user-setup'),
	
	
    path('login',views.login, name='login'),
    path('logout',views.logout, name='logout'),

	#02Feb
	path('brta',views.brta_dashboard, name='brta'),
	path('bus-owner',views.bus_owner_dashboard, name='bus-owner'),
	path('emergency-service',views.emergency_service_dashboard, name='emergency-service'),
	path('urban-authority',views.urban_dashboard, name='urban-authority'),

]

